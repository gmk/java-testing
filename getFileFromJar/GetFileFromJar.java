import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;


public class GetFileFromJar {
	public static void main(String args[]) throws IOException {
		String line;

		String myFile = "file.txt";

		InputStream is = GetFileFromJar.class.getResourceAsStream("resources/" + myFile);
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);

		// Do something w/ file contents
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
	}
}
